<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'yii\\swiftmailer\\' => array($vendorDir . '/yiisoft/yii2-swiftmailer'),
    'yii\\redis\\' => array($vendorDir . '/yiisoft/yii2-redis/src'),
    'yii\\queue\\sync\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/sync'),
    'yii\\queue\\redis\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/redis'),
    'yii\\queue\\gearman\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/gearman'),
    'yii\\queue\\file\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/file'),
    'yii\\queue\\db\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/db'),
    'yii\\queue\\beanstalk\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/beanstalk'),
    'yii\\queue\\amqp_interop\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/amqp_interop'),
    'yii\\queue\\amqp\\' => array($vendorDir . '/yiisoft/yii2-queue/src/drivers/amqp'),
    'yii\\queue\\' => array($vendorDir . '/yiisoft/yii2-queue/src'),
    'yii\\imagine\\' => array($vendorDir . '/yiisoft/yii2-imagine'),
    'yii\\httpclient\\' => array($vendorDir . '/yiisoft/yii2-httpclient/src'),
    'yii\\gii\\' => array($vendorDir . '/yiisoft/yii2-gii'),
    'yii\\faker\\' => array($vendorDir . '/yiisoft/yii2-faker'),
    'yii\\debug\\' => array($vendorDir . '/yiisoft/yii2-debug'),
    'yii\\composer\\' => array($vendorDir . '/yiisoft/yii2-composer'),
    'yii\\bootstrap\\' => array($vendorDir . '/yiisoft/yii2-bootstrap/src'),
    'yii\\authclient\\' => array($vendorDir . '/yiisoft/yii2-authclient/src'),
    'yii\\' => array($vendorDir . '/yiisoft/yii2'),
    'yidashi\\markdown\\' => array($vendorDir . '/yidashi/yii2-bootstrap-markdown'),
    'xj\\oauth\\' => array($vendorDir . '/xj/yii2-oauth'),
    'phpDocumentor\\Reflection\\' => array($vendorDir . '/phpdocumentor/reflection-common/src', $vendorDir . '/phpdocumentor/type-resolver/src', $vendorDir . '/phpdocumentor/reflection-docblock/src'),
    'linslin\\yii2\\curl\\' => array($vendorDir . '/linslin/yii2-curl'),
    'kartik\\time\\' => array($vendorDir . '/kartik-v/yii2-widget-timepicker'),
    'kartik\\select2\\' => array($vendorDir . '/kartik-v/yii2-widget-select2'),
    'kartik\\daterange\\' => array($vendorDir . '/kartik-v/yii2-date-range'),
    'kartik\\date\\' => array($vendorDir . '/kartik-v/yii2-widget-datepicker'),
    'kartik\\color\\' => array($vendorDir . '/kartik-v/yii2-widget-colorinput'),
    'kartik\\base\\' => array($vendorDir . '/kartik-v/yii2-krajee-base'),
    'jianyan\\websocket\\' => array($vendorDir . '/jianyan74/yii2-websocket/src'),
    'jianyan\\basics\\' => array($vendorDir . '/jianyan74/rageframe-basics'),
    'e282486518\\migration\\' => array($vendorDir . '/e282486518/yii2-console-migration'),
    'dosamigos\\datetimepicker\\' => array($vendorDir . '/2amigos/yii2-date-time-picker-widget/src'),
    'denisog\\cronjobs\\' => array($vendorDir . '/denisogr/yii2-cronjobs'),
    'crazyfd\\qiniu\\' => array($vendorDir . '/crazyfd/yii2-qiniu'),
    'crazydb\\ueditor\\' => array($vendorDir . '/crazydb/yii2-ueditor'),
    'cebe\\markdown\\' => array($vendorDir . '/cebe/markdown'),
    'Webmozart\\Assert\\' => array($vendorDir . '/webmozart/assert/src'),
    'Symfony\\Polyfill\\Php70\\' => array($vendorDir . '/symfony/polyfill-php70'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Polyfill\\Apcu\\' => array($vendorDir . '/symfony/polyfill-apcu'),
    'Symfony\\Component\\Yaml\\' => array($vendorDir . '/symfony/yaml'),
    'Symfony\\Component\\Process\\' => array($vendorDir . '/symfony/process'),
    'Symfony\\Component\\HttpFoundation\\' => array($vendorDir . '/symfony/http-foundation'),
    'Symfony\\Component\\Finder\\' => array($vendorDir . '/symfony/finder'),
    'Symfony\\Component\\EventDispatcher\\' => array($vendorDir . '/symfony/event-dispatcher'),
    'Symfony\\Component\\DomCrawler\\' => array($vendorDir . '/symfony/dom-crawler'),
    'Symfony\\Component\\Debug\\' => array($vendorDir . '/symfony/debug'),
    'Symfony\\Component\\CssSelector\\' => array($vendorDir . '/symfony/css-selector'),
    'Symfony\\Component\\Console\\' => array($vendorDir . '/symfony/console'),
    'Symfony\\Component\\Cache\\' => array($vendorDir . '/symfony/cache'),
    'Symfony\\Component\\BrowserKit\\' => array($vendorDir . '/symfony/browser-kit'),
    'Symfony\\Bridge\\PsrHttpMessage\\' => array($vendorDir . '/symfony/psr-http-message-bridge'),
    'Psr\\SimpleCache\\' => array($vendorDir . '/psr/simple-cache/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log/Psr/Log'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
    'Psr\\Container\\' => array($vendorDir . '/psr/container/src'),
    'Psr\\Cache\\' => array($vendorDir . '/psr/cache/src'),
    'Overtrue\\Socialite\\' => array($vendorDir . '/overtrue/socialite/src'),
    'Overtrue\\Pinyin\\' => array($vendorDir . '/overtrue/pinyin/src'),
    'Omnipay\\WechatPay\\' => array($vendorDir . '/lokielse/omnipay-wechatpay/src'),
    'Omnipay\\UnionPay\\' => array($vendorDir . '/lokielse/omnipay-unionpay/src'),
    'Omnipay\\Alipay\\Tests\\' => array($vendorDir . '/lokielse/omnipay-alipay/tests'),
    'Omnipay\\Alipay\\' => array($vendorDir . '/lokielse/omnipay-alipay/src'),
    'OSS\\' => array($vendorDir . '/aliyuncs/oss-sdk-php/src/OSS'),
    'Monolog\\' => array($vendorDir . '/monolog/monolog/src/Monolog'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'GuzzleHttp\\Promise\\' => array($vendorDir . '/guzzlehttp/promises/src'),
    'GuzzleHttp\\' => array($vendorDir . '/guzzlehttp/guzzle/src'),
    'Faker\\' => array($vendorDir . '/fzaninotto/faker/src/Faker'),
    'Egulias\\EmailValidator\\' => array($vendorDir . '/egulias/email-validator/EmailValidator'),
    'EasyWeChat\\' => array($vendorDir . '/overtrue/wechat/src'),
    'EasyWeChatComposer\\' => array($vendorDir . '/easywechat-composer/easywechat-composer/src'),
    'Doctrine\\Instantiator\\' => array($vendorDir . '/doctrine/instantiator/src/Doctrine/Instantiator'),
    'DeepCopy\\' => array($vendorDir . '/myclabs/deep-copy/src/DeepCopy'),
    'Da\\QrCode\\' => array($vendorDir . '/2amigos/qrcode-library/src'),
    'Codeception\\PHPUnit\\' => array($vendorDir . '/codeception/phpunit-wrapper/src'),
    'Codeception\\Extension\\' => array($vendorDir . '/codeception/base/ext'),
    'Codeception\\' => array($vendorDir . '/codeception/stub/src', $vendorDir . '/codeception/base/src/Codeception'),
);
